#!/home/pi/hmi/flask/bin/python

import spidev
import time
spi = spidev.SpiDev()
spi.open(0,0)
chan = 1
while True:
    try:
        response = spi.xfer2([1, 128+(chan << 4), 0])
        print(response)
        value = (response[1] & 3) * 256 + response[2]
        print value
        time.sleep(1)
    except KeyboardInterrupt:
        spi.close()