#!/home/pi/hmi/flask/bin/python

# Wird automatisch gestartet /etc/rc.local

import sys
from app import app
app.run(host="0.0.0.0",
        port=8080,
        debug=False)

sys.dont_write_bytecode = False

