# coding: utf-8
import os
WTF_CSRF_ENABLED = False
SECRET_KEY = 'robotic'
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'mysql://root:robotic@localhost/hmi'
SQLALCHEMY_ECHO = False
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

MAIL_SERVER = 'mail.rfh-campus.de'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = ""
MAIL_PASSWORD = ""