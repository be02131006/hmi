# coding: utf-8
from flask.ext.wtf import Form
from wtforms import StringField, SelectField, HiddenField, PasswordField
from wtforms.validators import DataRequired


class FrmSensor(Form):
    name = StringField('Sensorname', validators=[DataRequired()])
    sensorid = HiddenField('Sensorid', validators=[DataRequired])
    inputrange = SelectField('Eingang', validators=[DataRequired], choices=[('010', '0...10V'),
                                                                            ('210', '2...10V'),
                                                                            ('050', '0...5V'),
                                                                            ('020', '0...20mA'),
                                                                            ('420', '4...20mA')])
    channel = SelectField('Channel', validators=[DataRequired], choices=[('0', '1'),
                                                                         ('1', '2'),
                                                                         ('2', '3'),
                                                                         ('3', '4')])

    unit = StringField('Einheit', validators=[DataRequired])
    rangemin = StringField('Eingang min', validators=[DataRequired])
    rangemax = StringField('Eingang max', validators=[DataRequired])
    scalemin = StringField('Scala min', validators=[DataRequired])
    scalemax = StringField('Scala max', validators=[DataRequired])

class LoginForm(Form):
    benutzer = StringField('Benutzer', validators=[DataRequired])
    passwort = PasswordField('Passwort', validators=[DataRequired])