# coding: utf-8

from app import app
from collections import namedtuple

import subprocess
import netifaces

# https://pypi.python.org/pypi/netifaces
# https://wifi.readthedocs.org/en/latest/

# https://pypi.python.org/pypi/python-networkmanager
#       $ sudo apt-get install python-dbus
#       $ virtualenv --system-site-packages dbus-venv
#       /home/pi/hmi/flask/bin/pip install dbus-python
#       sudo apt-get install libdbus-glib-1-dev
class Network():

    def __init__(self):
        self.ifaces = netifaces.interfaces()
        self.lan = ""
        self.wlan = ""
        print self.ifaces
        self.__getip()


    def __getip(self):
        for iface in self.ifaces:
            if iface.startswith('eth'):
                try:
                    print(iface)
                    addrs = netifaces.ifaddresses(iface)
                    addrs_list = addrs[netifaces.AF_INET]
                    if len(addrs_list)>0:
                        for entry in addrs_list:
                            # print entry['addr']
                            #self.lan.append(entry['addr'])
                            self.lan = entry['addr']
                    # addrs = None
                except:
                    pass
            elif iface.startswith('wlan'):
                try:
                    print(iface)
                    addrs = netifaces.ifaddresses(iface)
                    addrs_list = addrs[netifaces.AF_INET]
                    if len(addrs_list)>0:
                        for entry in addrs_list:
                            # print entry['addr']
                            # self.wlan.append(entry['addr'])
                            self.wlan = entry['addr']
                    # addrs = None
                except:
                    pass


    def __getwlanip(self):
        pass


class WpaSupplicantConfiguration(object):

    def getwpaconf(self):
        konfiguration = False
        networkcount = 0
        dictnetwork = dict()
        n=dict
        command = "sudo more /etc/wpa_supplicant/wpa_supplicant.conf"
        p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        for line in out.split('\n'):
            if line.startswith('network={'):
                print line
                networkcount += networkcount

            elif line.startswith('ssid='):
                    print line
                    ssid = str(line).strip('=')
                    print ssid

                    n["ssid"] = ssid

            elif line.startswith('psk='):
                    print line
                    psk = line.strip("=")
                    psk = psk[1][1:-1]
                    n["psk"] = psk
                    print(psk)
            elif line.startswith('}'):
                    print line
                    konfiguration = False
                    dictnetwork[networkcount]=n
                    n = None
            else:
                pass

        return True


class HostName(object):
    def __init__(self):
        self.hostname = self.getname()

    def setname(self, arg):
        command="sudo sed -i 's/127.0.1.1    %s/127.0.1.1    %s/' /etc/hosts"%(self.hostname, arg)
        p=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        p.communicate()

        command="sudo sed -i 's/%s/%s/' /etc/hostname"%(self.hostname, arg)
        p=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        p.communicate()

        command="sudo reboot"
        p=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        p.communicate()

        return "OK"

    def getname(self):
        command='more /etc/hostname'
        p=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        out, err =  p.communicate()
        hostname = str(out.split('\n')[3])
        return hostname
