from flask import Flask, session#, jsonify
# from werkzeug.exceptions import default_exceptions, HTTPException
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.mail import Mail

app = Flask(__name__)

mail = Mail(app)
app.secret_key='robotic'
app.config.from_object('config')
db = SQLAlchemy(app)
lm = LoginManager()
lm.init_app(app)

from app import views, db_model

