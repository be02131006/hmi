function loadpage(url) {
    $.get(url, function(newpage) {
        $('body').html(newpage);
    })
}
/*
$('a').click( function(){

    var ahref = $(this).attr('href');
    console.log(ahref);
    loadpage(ahref);
    return false;
});
*/
function submit_form(form){
    /*
    var csrftoken = form.csrf_token.value;
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
        }
    });
    */

    //var myurl = $(form).attr('action');
    //console.log(myurl);
    $.ajax({
        type: 'POST',
        data: $(form).serialize(),
        dataType: 'JSON',
        url: $(form).attr('action'),
        success: function(res) {
            //console.log("success");
            //var obj = jQuery.parseJSON( res );
            var obj = res;
            //console.log(obj.redirect);
            if (obj.redirect != ""){
                loadpage(obj.redirect);
            }
        },
        error: function(error) {
            console.log("error");
            console.log(error);
        }
    });
}


$("input[type='text']").keyboard({
    language     : 'de',
    layout       : 'german-qwertz-1'
});

$("input[type='password']").keyboard({
    language     : 'de',
    layout       : 'german-qwertz-1'
});

$("textarea").keyboard({
    language     : 'de',
    layout       : 'german-qwertz-1'
});