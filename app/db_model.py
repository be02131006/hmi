# coding: utf-8
from app import db


class dbSensor(db.Model):

    __tablename__ = 'sensor'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    unit = db.Column(db.String(64), index=True, unique=False)
    channel = db.Column(db.String(1), index=True, unique=True)
    inputrange = db.Column(db.String(3), index=True, unique=False)
    rangemin = db.Column(db.String(16), index=True, unique=False)
    rangemax = db.Column(db.String(16), index=True, unique=False)
    scalemin = db.Column(db.String(16), index=True, unique=False)
    scalemax = db.Column(db.String(16), index=True, unique=False)
    values = db.relationship('dbSensorValues', backref='sensorv', lazy='dynamic')

    def __init__(self, name):
        self.name = name


    def __repr__(self):
        return ("Sensor id: %s, name: %s, unit: %s, channel: %s, rangemin: %s, rangemax: %s, scalemin: %s, scalemax: "
        "%s"%(self.id, self.name, self.unit, self.channel, self.rangemin, self.rangemax, self.scalemin, self.scalemax))


class dbSensorValues(db.Model):
    __tablename__ = 'sensorvalues'
    id = db.Column(db.Integer, primary_key=True)
    sensor = db.Column(db.Integer, db.ForeignKey('sensor.id'))
    timestamp = db.Column(db.DateTime)
    value = db.Column(db.String(32), index=True, unique=True)


class dbeMail(db.Model):
    __tablename__ = 'email'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    senderaddress = db.Column(db.String(128), index=True, unique=True)
    receiveraddress = db.Column(db.String(128), index=True, unique=True)
    subject = db.Column(db.String(128), index=True, unique=True)
    serveraddress = db.Column(db.String(128), index=True, unique=True)
    serverport = db.Column(db.String(5), index=True, unique=True)
    password = db.Column(db.String(128), index=True, unique=True)
