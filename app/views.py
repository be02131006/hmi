# coding: utf-8
from app import app, db  # , mail  # , lm #, db_model
from flask import render_template, redirect, url_for, request, json, Response, jsonify, session  # ,send_file, make_response
# from flask.ext.login import LoginManager
from db_model import dbSensor
from forms import FrmSensor, LoginForm
import netzwerk
import spidev
import time
# import subprocess
import os
import email
import sys

from math import floor, ceil
from reportlab.platypus import Spacer, Image
from reportlab.platypus import Paragraph, Table, TableStyle, SimpleDocTemplate
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.lib.units import cm
import cStringIO

import base64

from flask.ext.mail import Message, Mail


@app.route('/login', methods=['GET','POST'])
def login():
    if 'angemeldet' in session:
        return redirect(url_for('index'))
    else:
        form = LoginForm()
        if request.method == "POST":
            session['benutzer'] = request.form['benutzer']
            session['passwort'] = request.form['passwort']
            session['angemeldet'] = True
            session['benutzer_email'] = str(session['benutzer']) + "@studmail.rfh-koeln.de"


            curl_string = "curl 'https://wlanweblogin.rfh-koeln.de:8081/vpn/loginUser' -H 'Host: " \
                          "wlanweblogin.rfh-koeln.de:8081' -H 'User-Agent: Mozilla/5.0 " \
                          "(X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0' -H " \
                          "'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H " \
                          "'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H 'DNT: 1' -H " \
                          "'Referer: https://wlanweblogin.rfh-koeln.de:8081/vpn/loginformWebAuth.html' -H " \
                          "'Connection: keep-alive' -H 'Content-Type: application/x-www-form-urlencoded' --data " \
                          "'userid=%s&password=%s&url=http://www.google.de&serverip='" % (session['benutzer'],
                                                                                          session['passwort'])
            try:
                os.system(curl_string)
            except:
                pass

            url = url_for('login')
            json_obj = jsonify(redirect=url)
            return(json_obj)
        else:
            title = {"title" : "",
                     "subtitle":""}
            return render_template('login.html', title=title, form=form)


@app.route('/logout', methods=['GET'])
def logout():
    session.pop('benutzer', None)
    session.pop('passwort', None)
    session.pop('angemeldet', None)
    return redirect(url_for('index'))


@app.route('/',  methods=['GET', 'POST'])
@app.route('/index',  methods=['GET', 'POST'])
def index():

    sensortuple = db.session.query(dbSensor).all()

    net = netzwerk.Network()
    session['lanip'] = net.lan
    session['wlanip'] = net.wlan
    wlan = netzwerk.WpaSupplicantConfiguration()
    wlan.getwpaconf()

    title = {"title" : "Sensoren",
            "subtitle":"Sensorliste"}
    return render_template("start.html", title=title, sensors=sensortuple)


@app.route('/neuersensor', methods=['POST', 'GET'])
def neuersensor():
    if request.method == "POST":
        name = request.form['name']
        neuer_sensor = dbSensor(name)
        try:
            db.session.add(neuer_sensor)
            db.session.commit()
        except Exception as inst:
            return json.dumps({'status':'Error','message':inst.message})
        newsensorid = str(neuer_sensor.id)
        url = url_for('sensor',sensorid = newsensorid)
        json_obj = jsonify(redirect=url)
        return(json_obj)
    if request.method == 'GET':
        form = FrmSensor()
        title = {"title" : "Sensoren",
                 "subtitle":"neuer Sensor"}
        return render_template("neuersensor.html", title=title, form=form)


@app.route('/sensor/<sensorid>')
def sensor(sensorid):
    try:
        sensor = dbSensor.query.get(int(sensorid))
    except Exception as inst:
        print inst

    title = {"title" : "",
             "subtitle":""}
    return render_template("sensor.html", sensor=sensor)
                           # title=title,



@app.route('/sensor_edit/<sensorid>', methods=['POST', 'GET'])
def sensor_edit(sensorid):
    form = FrmSensor()
    if request.method =='POST':
        print "Update Entry POST"
        try:
            dbResultSensor = dbSensor.query.get(form.sensorid.data)
            dbResultSensor.name = form.name.data
            dbResultSensor.channel = form.channel.data
            dbResultSensor.inputrange = form.inputrange.data
            dbResultSensor.unit = form.unit.data
            dbResultSensor.rangemin = form.rangemin.data
            dbResultSensor.rangemax = form.rangemax.data
            dbResultSensor.scalemin = form.scalemin.data
            dbResultSensor.scalemax = form.scalemax.data
            db.session.commit()
        except Exception as inst:
            print inst

        url = url_for('sensor', sensorid=form.sensorid.data)
        json_obj = jsonify(redirect=url)
        return(json_obj)

    if request.method == 'GET':

        try:
            dbResultSensor = dbSensor.query.get(int(sensorid))
        except Exception as inst:
            print inst

        form.sensorid.data = dbResultSensor.id
        form.name.data = dbResultSensor.name
        form.channel.data = dbResultSensor.channel
        form.inputrange.data = dbResultSensor.inputrange
        form.unit.data = dbResultSensor.unit
        form.rangemin.data = dbResultSensor.rangemin
        form.rangemax.data = dbResultSensor.rangemax
        form.scalemin.data = dbResultSensor.scalemin
        form.scalemax.data = dbResultSensor.scalemax

        title = {"title" : "Sensoren",
                "subtitle":"Sensoreinstellungen"}

        return render_template("sensor_edit.html", form=form, title=title, sensor=dbResultSensor)


@app.route('/sensor_delete/<sensorid>')
def sensor_delete(sensorid):
    sensor = dbSensor.query.get(int(sensorid))
    db.session.delete(sensor)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/sensor/graph/<sensor_id>/<delay_time>')
def sensor_graph(sensor_id, delay_time):
    yachse = dbSensor.query.get(int(sensor_id))
    spi_iface = spidev.SpiDev()
    spi_iface.open(0, 0)
    start_time = time.time()
    print("1"*30)

    def stream(iface, achse, delay):
        channel = int(achse.channel)
        y_rangemin = float(achse.rangemin)
        y_rangemax = float(achse.rangemax)
        y_scalemin = float(achse.scalemin)
        y_scalemax = float(achse.scalemax)
        while True:
            try:
                time_elapsed = str(time.time() - start_time)
                y_response = iface.xfer2([1, 128 + (channel << 4), 0])
                y_value_row = (y_response[1] & 3) * 256 + y_response[2]
                y_value = (((float(y_value_row)*y_rangemax)/float(1024))*(y_scalemax-y_scalemin))/(y_rangemax-y_rangemin)
                yield_string = "data:%s,%s\n\n" % (str(y_value), time_elapsed)
                print(yield_string)
                yield yield_string
                time.sleep(delay)
            except GeneratorExit:
                print("close spi")
                spi_iface.close()
            except IOError:
                print("IOError")
                break
            except:
                print "Unexpected error: ", sys.exc_info()[0]
    return Response(stream(iface=spi_iface, achse=yachse, delay=float(delay_time)), content_type='text/event-stream')


@app.route('/messung')
def messung():
    sensortuple = db.session.query(dbSensor).all()
    return render_template("graph.html", sensors=sensortuple)


@app.route('/messung/graph/<y_sensor_id>/<x_sensor_id>/<delay_time>')
def graph(y_sensor_id, x_sensor_id, delay_time):
    yachse = dbSensor.query.get(int(y_sensor_id))
    xachse = dbSensor.query.get(int(x_sensor_id))

    spi_iface = spidev.SpiDev()
    spi_iface.open(0, 0)

    def stream(iface, y_achse, x_achse, delay):
        y_channel = int(y_achse.channel)
        y_rangemin = float(y_achse.rangemin)
        y_rangemax = float(y_achse.rangemax)
        y_scalemin = float(y_achse.scalemin)
        y_scalemax = float(y_achse.scalemax)
        x_channel = int(x_achse.channel)
        x_rangemin = float(x_achse.rangemin)
        x_rangemax = float(x_achse.rangemax)
        x_scalemin = float(x_achse.scalemin)
        x_scalemax = float(x_achse.scalemax)
        while True:
            try:
                y_response = iface.xfer2([1, 128+(y_channel << 4), 0])
                y_value_row = (y_response[1] & 3) * 256 + y_response[2]

                x_response = iface.xfer2([1, 128+(x_channel << 4), 0])
                x_value_row = (y_response[1] & 3) * 256 + x_response[2]

                y_value = (((float(y_value_row)*y_rangemax)/float(1024))*(y_scalemax-y_scalemin))/(y_rangemax-y_rangemin)
                x_value = (((float(x_value_row)*x_rangemax)/float(1024))*(x_scalemax-x_scalemin))/(x_rangemax-x_rangemin)

                yield_string = "data:%s,%s\n\n" % (str(y_value), str(x_value))
                print(yield_string)
                yield yield_string
                time.sleep(delay)
            except GeneratorExit:
                print("close spi")
                spi_iface.close()
            except IOError:
                print("IOError")
                break
            except:
                print "Unexpected error: ", sys.exc_info()[0]
    return Response(
        stream(
            iface=spi_iface,
            y_achse=yachse,
            x_achse=xachse,
            delay=float(delay_time)),
        content_type='text/event-stream'
    )


@app.route('/report', methods=['POST', 'GET'])
def report():
    list_index = list()
    list_y = list()
    list_x = list()
    titel = ""
    beschreibung = ""
    img_string =""

    if request.method == "POST":
        datenstring = request.get_json(force=True)
        json_data = json.dumps(datenstring)
        json_data = json_data.encode('utf-8')
        decoded_json = json.loads(json_data)

        index_counter = 1
        for item in decoded_json:
            if item.get("image"):
                img_string = item.get("image")[22:]
            elif item.get("ydesc"):
                tbl_xdesc = str(item.get("xdesc").encode('utf-8'))
                tbl_ydesc = str(item.get("ydesc").encode('utf-8'))
                tbl_index = str("Bezeichnung")
            elif item.get('name'):
                titel = item.get('name')
            elif item.get("descr"):
                beschreibung = str(item.get("descr").encode('utf-8'))
            else:
                if (str(item.get("x")) != "None") and (str(item.get("y")) != "None"):
                    if str(item.get("x")) != "None":
                        print(item.get("x"))
                        list_x.append("%0.2f" % item.get("x"))
                    else:
                        print(item.get("x"))
                        list_x.append("%0.2f" % 0)

                    if str(item.get("y")) != "None":
                        print(item.get("y"))
                        list_y.append("%0.2f" % item.get("y"))
                    else:
                        print(item.get("y"))
                        list_y.append("%0.2f" % 0)

                    list_index.append(str(index_counter))
                    index_counter += 1

    output = cStringIO.StringIO()

    doc = SimpleDocTemplate("/home/pi/hmi/app/static/tmp/Kenndatenauswertung.pdf", pagesize=A4, )
    tbl_daten = [[tbl_index] + list_index, [tbl_xdesc] + list_x, [tbl_ydesc] + list_y]

    Story = []
    styles = getSampleStyleSheet()

    ptext = '<font size=20>%s</font>' % titel
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 22))

    ptext = '<font size=10>%s </font>' % time.strftime("%d.%m.%Y %H:%M:%S")
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 32))
    ptext = "<font size=12>%s</font>" % beschreibung
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 18))

    f = open("/home/pi/hmi/app/static/tmp/graph.png", "w")
    f.write(base64.decodestring(img_string))
    f.close()

    os.system("convert /home/pi/hmi/app/static/tmp/graph.png /home/pi/hmi/app/static/tmp/graph.jpeg")

    logo = "/home/pi/hmi/app/static/tmp/graph.jpeg"
    im = Image(logo, width=14*cm, height=14*cm, kind='proportional')
    Story.append(im)
    Story.append(Spacer(1, 32))

    max_number_of_cols = 10
    step = int(ceil(len(tbl_daten[0]) / max_number_of_cols))
    for i in range(0, step):
        data_section = []
        for d in tbl_daten:
            data_section.append(d[int(i * max_number_of_cols):int((i + 1) * max_number_of_cols)])
        t = Table(data_section)
        t.hAlign = 'LEFT'
        t.spaceBefore = 10
        t.spaceAfter = 10
        t.setStyle(TableStyle(
            [('BOX', (0, 0), (-1, -1), 0.5, colors.black),
             ('INNERGRID', (0, 0), (-1, -1), 0.5, colors.black)]))
        Story.append(t)

    Story.append(Spacer(1, 32))

    doc.build(Story)
    output.close()

    # Send eMail
    app.config.update(dict(
        DEBUG=False,
        MAIL_USERNAME=session['benutzer'],
        MAIL_PASSWORD=session['passwort'],
    ))


    subject = "Bericht"
    recipients = [session['benutzer_email']]
    sender = session['benutzer_email']
    text_body = "Bericht"

    mail = Mail(app)
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    with app.open_resource("/home/pi/hmi/app/static/tmp/Kenndatenauswertung.pdf") as fp:
        msg.attach("Kenndatenauswertung.pdf", "application/pdf", fp.read())

    mail.send(msg)

    os.remove("/home/pi/hmi/app/static/tmp/graph.png")
    os.remove("/home/pi/hmi/app/static/tmp/graph.jpeg")
    os.remove("/home/pi/hmi/app/static/tmp/Kenndatenauswertung.pdf")

    json_obj = jsonify(status="success")
    return json_obj

@app.route('/email')
def emaila():
    email.send("")

# Netzwerkeinstellungen
@app.route('/einstellungen')
def einstellungen():
    pass

@app.route('/halt')
def halt():
    os.system("sudo halt")
    pass