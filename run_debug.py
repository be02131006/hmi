#!/home/pi/hmi/flask/bin/python

import sys
from app import app
app.run(host="0.0.0.0",
        port=8080,
        debug=True)

sys.dont_write_bytecode = True

